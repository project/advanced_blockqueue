=========================================================
Advanced Blockqueue
=========================================================

The Drupal Blockmanagement is not very flexible, especially for news
sites where blocks and all kinds of content can be in a section.
This Module is actually inspired by http://drupal.org/project/blockqueue
with a main difference: The user has to place one block. This is
the placeholder for the Block Queue. On any page the user has now
the opportunity to manage this blockqueue. If there is a page
without blockqueue drupal jumps one hierarchy up and checks for
blocks and so on, until it has reached the frontpage.

The following elements are allowed in the blockqueue:
- nodes
- blockes
- reference to other blockqueues from other pages

This module is sponsered by www.previon.ch


============================================
How to use it
============================================
There is no configuration needed. The users with the permissions will
see on every page an icon in the top right corner where they can edit
the blockqueue for the page. Just add and remove images to the queue,
simple and straight forward.

The module provides a block "Advanced Blockqueue" you have to initially
place this block to an area of your choice. When editing the blocks on
your page they will ALWAYS be displayed in that block.

Sofar there is no possibility to have multiple Block Queues on one page.
Of course you can still always use the default drupal block management
for all the other regions.

============================================
Theming - How to
============================================
Just overwrite the following .tpl files depending on what kind of block/node
you want to changes.
The following types are available:
node, block and reference. This results in the following possible tpl.php files:
- advanced-blockqueue-block.tpl.php
- advanced-blockqueue-node.tpl.php
- advanced-blockqueue-reference.tpl.php