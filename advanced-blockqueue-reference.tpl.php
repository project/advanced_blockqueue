<?php
//$id;
/*
 * @file template for output of references in the nodequeue
 *
 */
?>

<div class="advanced-block <?php print $class; ?>">
  <?php if($title != 'Blockqueue'){?>
    <h2><?php print $title; ?></h2>
  <?php } ?>
  <?php print $content; ?>
</div>