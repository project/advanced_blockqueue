<?php

/**
 * @file
 */

/**
 * form definition for the admin settings
 */
function advanced_blockqueue_admin_form() {
  $form = array();

  $options = array(1 => t('Inherit blockvisibility'), 2 => t('Force blockvisibility'));

  $form['advanced_blockqueue_visibility'] = array(
    '#title' => t('Block visibility'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => variable_get('advanced_blockqueue_visibility', ''),
    '#description' => t('Forcing the blockvisibility will ignore any visibility settings made for a given block. ' . 
      'When using the Inherit option, the blockvisibility will be respected.'),
  );

  $form['advanced_blockqueue_exclude'] = array(
    '#title' => t('Exclude icon from the following pages'),
    '#type' => 'textarea',
    '#description' => t('Enter pages that won\'t have the Advanced Blockqueue Item in the upper right corner. You can user * as wildcart. One page per line.'),
    '#default_value' => variable_get('advanced_blockqueue_exclude', ''),
  );

  $form['advanced_blockqueue_styles'] = array(
    '#title' => t('Styles'),
    '#type' => 'textarea',
    '#description' => t('A node may have different styles. Lets take the following example. You have a nodetype that 
      contains a textfield, a video field and an image field. On page x you might want to display the video with the 
      text. On page y on the other hand you want to show the image only. Here you can set different style key words, 
      that will then show up in the blockqueue administration and can be caught in the .tpl.') . '<br/>'.
      t('Please enter one style per line. Without comma separation.'),
    '#default_value' => variable_get('advanced_blockqueue_styles', 'default'),
  );

  $form['advanced_blockqueue_enable_referencing'] = array(
    '#title' => t('Enable referencing option'),
    '#type' => 'checkbox',
    '#description' => t('If checked, other blockqueues may be referenced. WARNING: this functionality is still under development'),
    '#default_value' => variable_get('advanced_blockqueue_enable_referencing', ''),
  );

  $form['advanced_blockqueue_enable_autocompletion'] = array(
    '#title' => t('Enable autocompletion'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('advanced_blockqueue_enable_autocompletion', 1),
    '#description' => t('If checked the autocompletion option will be disabled'),
  );
  
  return system_settings_form($form);
}

function advanced_blockqueue_available_blocks_form() {
  $form = array();
  $abq = advanced_blockqueue_get_abq();
  
  $theme = variable_get('theme_default', 'garland');
  
  $form['advanced_blockqueue_block_availability_description'] = array(
    '#value' => t('Users that may set blocks in the Advanced Blockqueue are probably not interested in 
      most of the blocks that are provided by drupal and might even be confused. Here you can select, which
      blocks should be available for the given role to select.') . '<br/>' . 
      t('Your modifications will affect the %theme Theme, as it\'s set as the current front end theme.', array('%theme' => $theme)) .
      '<div class"clear"></div>',
  );
  
  $blocks_all = $abq->getBlocksInSystem(array(), 1);
  
  foreach (user_roles(FALSE, 'administer advanced blockqueue') as $key => $role) {
    $default = variable_get('advanced_blockqueue_block_availability_' . $key, array());
    $form['advanced_blockqueue_block_availability_' . $key] = array(
      '#type' => 'fieldset',
      '#title' => $role,
      '#tree' => TRUE,
      '#collapsible' => TRUE, 
      '#collapsed' => TRUE,
    );
    foreach ($blocks_all as $key2 => $block) {
      $form['advanced_blockqueue_block_availability_' . $key][$key2] = array(
        '#type' => 'checkbox',
        '#title' => $block,
        '#default_value' => $default[$key2],
      );
    }
  }
  $form['advanced_blockqueue_block_availability_divider'] = array(
    '#value' => '<div class="clear"></div>',
  );
  
  return system_settings_form($form);
}