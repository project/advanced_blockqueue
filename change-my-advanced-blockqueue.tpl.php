<?php
//$id;
/*
 * Created on 10.07.2009
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 * 
 * possible templates:
 * my-advanced-blockqueue-block.tpl.php
 * my-advanced-blockqueue-node.tpl.php
 */

?>


<?php
if($block['reftyp']==ADVANCED_BLOCKQUEUE_NODE):?>
	<div class="advanced-block <?php print strtolower($node->field_section[0]['value']); ?>">
		<?php if($title != 'Blockqueue'){?>

			<h2><?php print $title; ?></h2>
			<?php print l(theme('imagecache','artikel_rightside_coverimage',$node->field_artikel_cover_image[0]['filepath']),'node/'.$node->nid,array('html'=>TRUE));?>
			<?php print $node->field_artikel_teaser[0]['value']; ?>
		<?php } ?>
		<?php print $content; ?>
	</div>
<?php
else:
?>
	<div class="advanced-block">
		<?php if($title != 'Blockqueue'){?>
			<h2><?php print $title; ?></h2>
		<?php } ?>
		<?php print $content; ?>
</div>
<?php endif; ?>