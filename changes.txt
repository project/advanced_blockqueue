Advanced Blockqueue

2010/03/17
==========================
* Applied code conventions
* recursion control set to 6
* fixed an index problem in db

2009/12/14
==========================
* Possibility to control which blocks are available to choose from based on roles

2009/10/27
==========================
* fixed a bug, if a node path changes the blockqueue is being lost

2009/09/28
==========================
* improved capability to reference other nodequeues
* fixed some coding style issues

6.x-1.0-beta4 - 07.16.2009
==========================
- Improved and more flexible templating system

6.x-1.0-beta3 - 07.16.2009
==========================
- Bugfix: List ALL blocks when in edit mode

6.x-1.0-beta2
==========================
- Possibility to have an empty blockqueue
- Some usability issues