<?php
//$id;
/*
 * @file template for output of nodes in the nodequeue
 *
 * possible variables:
 *  $style: if there are styles defined
 *  $title: Title of the node
 *  $node: a normal node object
 */
?>

<div class="advanced-block <?php print $class; ?>">
	<?php if($title != 'Blockqueue'){?>
		<h2><?php print $title; ?></h2>
	<?php } ?>
	<?php print $content; ?>
</div>