<?php

class advanced_blockqueue {
  static private $instance = null;

  /**
   * We are implementing a singleton pattern
   */
  private function __construct() {
  }

  public function getInstance() {
    if (is_null(self :: $instance)) {
      self :: $instance = new self;
    }
    return self :: $instance;
  }

  /**
   * check if there is already a blockqueue for a give path
   * if there is not blockqueue, create one and return the bqid
   * @param string $path
   *
   */
  private function createBlockqueueIfNotExists($path) {
    global $language;

    $bqs = $this->getNodequeue($path);
    if (!$bqs) {
      $dbObject = array (
				'path' => $path,
				'lang' => $language->language,
      );

      drupal_write_record('advanced_blockqueue', $dbObject);
      return db_last_insert_id('advanced_blockqueue', 'bqid');
    }
    return $bqs->bqid;
  }

  /**
   *	Save or update a block to a blockqueue. If there's no queue yet
   *	a queue will be generated
   */
  public function addBlock($path, $refid, $weight = 0, $options = array ()) {
    $dbObject['path'] = drupal_get_path_alias(str_replace('$', '/', $path));

    $dbObject['bqid'] = $this->createBlockqueueIfNotExists($dbObject['path']);

    if ($this->isInBlockQueue($dbObject['bqid'], $refid)) {
      drupal_set_message(t('This element is already in the queue'));
      return;
    }
    if (!isset ($options['reftyp'])) {
      $options['reftyp'] = ADVANCED_BLOCKQUEUE_BLOCK;
    }
    if ($options['reftyp'] == ADVANCED_BLOCKQUEUE_REFERENCE) {
      $ref_queue = $this->getNodequeue($refid);
      $dbObject['refid'] = $ref_queue->bqid;
    }
    elseif ($options['reftyp'] == ADVANCED_BLOCKQUEUE_NODE) {
      $dbObject['refid'] = $refid;
    }
    elseif ($options['reftyp'] == ADVANCED_BLOCKQUEUE_MULTI) {
      $dbObject['refid'] = time(); //refid is usually referencing a block/node in the blocks table, since this is not a real block
    }
    else {
      $dbObject['refid'] = $refid;
    }

    $dbObject['reftyp'] = $options['reftyp'];
    drupal_write_record('advanced_blockqueue_elements', $dbObject);
  }

  /**
   * Remove a normal element from the blockqueue
   * @param int $beid
   */
  public function deleteBlockFromQueue($beid) {
    db_query("DELETE FROM {advanced_blockqueue_elements} WHERE beid=%d OR pbeid=%d", $beid, $beid);
  }

  /**
   * remove a sub element from a multielement block.
   * @param int $refid
   * 	this is most likely going to be the nid
   * @param int $beid
   * 	this is the block element id (actually the parent id of the subelement)
   */
  public function removeSub($refid, $beid) {
    db_query("DELETE FROM {advanced_blockqueue_elements} WHERE pbeid=%d AND refid=%d", $beid, $refid);
  }

  /**
   * check if an element is already in the queue
   * @param int $bqid
   * @param int $refid
   */
  private function isInBlockQueue($bqid, $refid) {
    global $language;
    $row = db_fetch_array(db_query("SELECT count(bqid) as c FROM {advanced_blockqueue_elements}
				WHERE bqid=%d AND refid=%d", $bqid, $refid));
    if ($row['c'] > 0) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * saving the order of blocks -> this is mainly to get
   * the weight of the blocks correctly
   */
  public function saveBlocks($form) {

    $dbObject->path = drupal_get_path_alias(str_replace('$', '/', $form['values']['path']));
    $dbObject->noblocks = $form['values']['noblocks'];

    $dbObject->bqid = $this->createBlockqueueIfNotExists($dbObject->path);
    drupal_write_record('advanced_blockqueue', $dbObject, array('bqid'));

    foreach ($form['values'] as $key => $val) {
      if ($val['id'] == 'abqe') {
        db_query("UPDATE {advanced_blockqueue_elements} " .
				"SET weight = %d, style = '%s' " .
				"WHERE beid = %d ", $val['weight'], $val['style'], $val['beid']);
        if (!empty($val['multi'])) { //incase we have subelements
          $ar_sub = explode(",", $val['multi']);
          $dbObject->pbeid = $val['beid'];
          $dbObject->reftyp = ADVANCED_BLOCKQUEUE_NODE;
          foreach ($ar_sub as $item) {
            $dbObject->refid = trim($item);
            $dbObject->style = $val[$item]['style'];
            drupal_write_record('advanced_blockqueue_elements', $dbObject);
          }
        }

        //we go through the existing multielements and make sure we set the right style
        if (!empty($val['existing_multi'])) {
          $ar_sub = explode(",", $val['existing_multi']);
          foreach ($ar_sub as $item) {
            db_query("UPDATE {advanced_blockqueue_elements} SET style=%d WHERE pbeid=%d AND refid=%d",
            $val[$item]['style'], $val['beid'], $item);
          }
        }
      }
    }
  }

  /**
   * get all the blocks for a given path and return an array with all
   * the needed block information
   *
   * @param string $path - path of location of the block
   *
   * @param array $options - some options in an associative array
   *
   * possible values are:
   * 		non_rec - if set to TRUE, the function will not check recursively for blocks
   * 				  this can be used if a block list should be displayed
   * 		child - the path last visited -> can be used to compare the new path with the child path
   * 				to avoid deadly loops
   * 		edit - flag to set to say that we are in edit mode
   * 		info - if set to true, the module will not get the title of the block, but instead the
   * 			   "info title" will be returned -> for example the "powered by drupal" - block
   * 			   doesn't have a title for display, it though has a title when it's being listed
   * 			   in the block list.
   *
   *
   */
  public function getBlocks($path, $options = array ()) {
    global $user,$language;

    //this is for savety reasons -> stop endless loops
    static $abq_recursion_controller = 0;

    $abq_recursion_controller++;
    if ($abq_recursion_controller > 6) {
      return array ();
    }
    $path = drupal_get_path_alias($path);

    //incase we don't want to show any blocks, let's leave
    $abq = db_fetch_object(db_query("SELECT noblocks FROM {advanced_blockqueue} WHERE path='%s'",$path));
    if($abq->noblocks){
      return array();
    }

    $result = db_query("SELECT abe.style,path,beid,abe.refid, abe.reftyp,abe.weight,ab.noblocks,ab.bqid " .
		"FROM {advanced_blockqueue} AS ab, " .
		"{advanced_blockqueue_elements} AS abe " .
		"WHERE ab.path = '%s' " .
		"AND ab.bqid = abe.bqid " .
		"AND (ab.lang='%s' OR ab.lang IS NULL) " .
        "AND abe.pbeid = 0 " .
		"ORDER BY weight asc", $path, $language->language);

    $blocks = array ();
    while ($row = db_fetch_object($result)) {
      $blocks[$row->refid] = array (
				'refid' => $row->refid,
				'reftyp' => $row->reftyp,
				'weight' => $row->weight,
				'beid' => $row->beid,
			  'style' => $row->style,
      );
      switch ($row->reftyp) {
        //name: is just for the list
        //title: is the actual block title
        //content: the content of the block
        case ADVANCED_BLOCKQUEUE_BLOCK :

          //if we are in edit mode we want to see the all the blocks in the list
          if($options['edit'] || variable_get('advanced_blockqueue_visibility', 1) != 1){
            $row2 = db_fetch_object(db_query("SELECT * FROM {blocks} AS b " .
								"WHERE b.bid=%d " .
								"", $row->refid));
          }
          else{ //for actually viewing the page we need to respect user visibility
            $rids = array_keys($user->roles);
            $row2 = db_fetch_object(db_query(db_rewrite_sql(("SELECT DISTINCT b.* FROM {blocks} b LEFT JOIN {blocks_roles} r ON b.module = r.module " .
    							"AND b.delta = r.delta " .
    							"WHERE  " .
    							"b.bid = $row->refid " .
    							"AND (r.rid IN (". db_placeholders($rids) .") " .
    							"OR r.rid IS NULL) " .
    							"ORDER BY b.region, b.weight, b.module"), 'b', 'bid'),array_merge( $rids)));

          }


          // we first check if we maybe already got something in the cache
          if (!count(module_implements('node_grants_HACK')) && $_SERVER['REQUEST_METHOD'] == 'GET' && ($cid = _block_get_cache_id($row2)) && ($cache = cache_get($cid, 'cache_block'))) {
            $tmp = $cache->data;
          }
          // if not we have to do the expensive work and put it into the cache
          else {
            if($options['info']){
              $tmp = module_invoke($row2->module, 'block', 'list');
              $tmp['subject'] = $tmp[$row2->delta]['info'];
            }
            else{
              $tmp = module_invoke($row2->module, 'block', 'view', $row2->delta);
            }
            $blocks[$row->refid]['class'] = 'block-'.$row2->module.'-'.$row2->delta;

            if (empty ($tmp['subject'])) { //incase there's a block that was manually generated
              $tmp['subject'] = $row2->title;
            }
            if (isset($cid)) {
              cache_set($cid, $tmp, 'cache_block', CACHE_TEMPORARY);
            }
            /*$array = module_invoke($block->module, 'block', 'view', $block->delta);
             if (isset($cid)) {
             cache_set($cid, $array, 'cache_block', CACHE_TEMPORARY);
             }*/
          }







          // Match path if necessary
          if($options['edit']){
            $page_match = TRUE;
          }
          elseif (($row2->pages || $row2->rid) && variable_get('advanced_blockqueue_visibility', 1) == 1) {//do we inherit the visibility

            if ($row2->visibility < 2) {
              $path = drupal_get_path_alias($_GET['q']);
              // Compare with the internal and path alias (if any).
              $page_match = drupal_match_path($path, $row2->pages);
              if ($path != $_GET['q']) {
                $page_match = $page_match || drupal_match_path($_GET['q'], $row2->pages);
              }
              // When $block->visibility has a value of 0, the block is displayed on
              // all pages except those listed in $block->pages. When set to 1, it
              // is displayed only on those pages listed in $block->pages.
              $page_match = !($row2->visibility xor $page_match);
            }
            else {
              $page_match = drupal_eval($row2->pages);
            }
          }
          else {
            $page_match = TRUE;
          }

          if ($page_match) { //Display the block
            $blocks[$row->refid]['name'] = $tmp['subject'];
            $blocks[$row->refid]['title'] = $tmp['subject'];
            $blocks[$row->refid]['content'] = $tmp['content'];
            $blocks[$row->refid]['block'] = $row2;
          } else {
            unset ($blocks[$row->refid]);
          }

          break;
        case ADVANCED_BLOCKQUEUE_REFERENCE :
          $bq = $this->getNodequeueById($row->refid);
          $blocks[$row->refid]['name'] = $bq->path . " [Blockqueue Reference]";
          $blocks[$row->refid]['title'] = 'Blockqueue';
          $blocks[$row->refid]['subblocks'] = $this->getBlocks($bq->path);
          $blocks[$row->refid]['path'] = $bq->path;
          break;
        case ADVANCED_BLOCKQUEUE_NODE :
          // we first check if we maybe already got something in the cache
          if (!count(module_implements('node_grants_HACK')) && $_SERVER['REQUEST_METHOD'] == 'GET' && ($cache = cache_get('block_node_'.$row->refid, 'cache_advanced_blockqueue'))) {
            $blocks[$row->refid] = $cache->data;
          }
          else {
            $node = node_load($row->refid);
            $blocks[$row->refid]['name'] = $node->title . " [Node " . $row->refid . "]";
            $blocks[$row->refid]['title'] = l($node->title, 'node/' . $node->nid);
            $blocks[$row->refid]['content'] = $node->teaser;
            $blocks[$row->refid]['node'] = $node;
            $blocks[$row->refid]['path'] = 'node/'. $node->nid;
            cache_set('block_node_'.$row->refid, $blocks[$row->refid], 'cache_advanced_blockqueue', time()+60*60*24);
          }
          break;
        case ADVANCED_BLOCKQUEUE_MULTI :
          $blocks[$row->refid]['name'] = t('Multielement Block');
          $blocks[$row->refid]['title'] = t('Multielement Block');
          $result3 = db_query("SELECT refid, style FROM {advanced_blockqueue_elements} WHERE pbeid = %d", $row->beid);
          while ($row3 = db_fetch_object($result3)) {
            $node = node_load($row3->refid);
            $node->bq_style = $row3->style;
            $blocks[$row->refid]['content'][$row3->refid] = $node;
          }
          break;
      }
    }

    if ($options['non_rec']) { //if non-recursive let's stop here
      return $blocks;
    }

    $arPath = explode("/", $path);
    unset ($arPath[sizeof($arPath) - 1]);
    $newPath = implode('/', $arPath);

    //$this->dbg("1 path: ".$path." - new Path: ".$newPath." - child: ".$options['child']." - child str replace: ".str_replace('$', '/', $options['child']));

    if (str_replace('$', '/', $options['child']) == variable_get('site_frontpage', 'node')) { // avoid a deadly loop
      return $blocks;
    }
    if (($newPath == '' || $newPath == '/') && sizeof($blocks) == 0) { //incase the path is empty -> no heritage could be found -> take frontpage
      return $this->getBlocks(variable_get('site_frontpage', 'node'), array (
				'child' => $path
      ));
    }
    if (sizeof($blocks) == 0) { //incase no blockqueue is definied -> look in the parent path
      $blocks = $this->getBlocks($newPath, array (
				'child' => $path
      )); //go recursively up
    }
    return $blocks;
  }
  private function dbg($str) {
    print $str . '<br/>';
  }
  /**
   * get a queue based on the path
   */
  public function getNodequeue($path) {
    global $language;

    $result = db_query("SELECT bqid,path FROM {advanced_blockqueue} WHERE path ='%s'
					AND (lang='%s' OR lang IS NULL) LIMIT 0,1", $path,$language->language);
    $row = db_fetch_object($result);
    if ($row == "" || $row == NULL) {
      return FALSE;
    }
    return $row;
  }

  /**
   * get a queue based on the bqid
   */
  public function getNodequeueById($id) {
    global $language;
    $result = db_query("SELECT bqid,path FROM {advanced_blockqueue} WHERE bqid =%d
					AND (lang='%s' OR lang IS NULL) LIMIT 0,1", $id, $language->language);
    $row = db_fetch_object($result);
    if ($row == "" || $row == NULL) {
      return FALSE;
    }
    return $row;
  }

  /**
   * autocomplete the path of existing block queues
   * @param string $string
   */
  public function autocomplete($string, $type = 'bq_ref') {
    global $language;
    $lang = $language->language;

    $matches = array ();
    switch ($type) {
      case 'bq_ref' :
        $result = db_query_range("SELECT bq.path FROM {advanced_blockqueue} bq WHERE LOWER(bq.path) LIKE LOWER('%s%%')", $string, 0, 10);
        while ($abq = db_fetch_object($result)) {
          $matches[$abq->path] = check_plain($abq->path);
        }
        print drupal_to_js($matches);
        exit ();

      case 'node' :
        $string = check_plain($string);
        if(module_exists('locale')){
          $result = db_query_range("SELECT title,nid FROM {node} bq WHERE LOWER(title) LIKE LOWER('%$string%')
							AND language='%s' OR language=''",$lang, 0, 10);
        }
        else{
          $result = db_query_range("SELECT title,nid FROM {node} bq WHERE LOWER(title) LIKE LOWER('%$string%')", 0, 10);
        }
        while ($abq = db_fetch_object($result)) {
          $matches[$abq->nid] = check_plain($abq->title);
        }
        print drupal_to_js($matches);
        exit ();
    }

  }

  /**
   * Return all blocks available in the system
   * @param array $existing_blocks
   *  an array of bid that should not be included. This is used when we are on the page where we
   *  add blocks to a page and we already have blocks placed in the queue. Those blocks should not
   *  be displayed in the select form anymore
   * @return array
   *  an array of blocks
   */
  public function getBlocksInSystem($existing_blocks=array(), $uid=0) {
    global $user;

    $theme = variable_get('theme_default', 'garland');

    if (!$uid) {
      $uid = $user->uid;
    }

    /*if (($cache = cache_get('')) && !empty($cache->data)) {
     $ar_special = $cache->data;
     }
     else {*/
    $option = array();

    // we go and get all the blocks from the db and get the information
    $result = db_query("SELECT * FROM {blocks} AS b WHERE b.theme = '%s'", $theme);
    $blocks_all = array();
    while ($row = db_fetch_array($result)) {
      $blocks = module_invoke($row['module'], 'block', 'list');

      $blocks_all[$row['bid']] = array(
          'info' => $blocks[$row['delta']]['info'],
          'title' => $row['title'],
          'bid' => $row['bid'],
      );
    }
    usort($blocks_all, '_advanced_blockqueu_sort_callback');//put them in alphabetical order

    // we are going to show only blocks that we have specified
    // we check for the users roles and add the blocks to display
    $blocksToDisplay = array();
    foreach ($user->roles as $key => $role) {
      $default = variable_get('advanced_blockqueue_block_availability_'.$key, array());
      foreach ($default as $key2 => $item) {
        if ($item) {
          $blocksToDisplay[] = $key2;
        }
      }
    }

    // we remove elements that are already in a queue
    foreach ($blocks_all as $v) {
      if (!array_key_exists($v['bid'], $existing_blocks) &&//filter blocks that are already in the queue
      (in_array($v['bid'], $blocksToDisplay) || $uid == 1)) { //only blocks we have permission to display
        $option[$v['bid']] = $v['info'];
      }
    }
    /*}*/
    return $option;
  }
}